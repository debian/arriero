#!/bin/sh
# This script is intended to be run as:
# arriero exec -x "update.sh -m 'Commit message' script [args...]" [packages]

commit_message=''
wns_opt=''
ignore_new=''
while [ "$#" -ge 1 ]; do
    case "$1" in
        -m)
            shift
            commit_message="$1"
            shift
            ;;
        --wrap-and-sort-opt|-w)
            shift
            wns_opt="$1"
            shift
            ;;
        --ignore-new)
            shift
            ignore_new='yes'
            ;;
        *)
            break
            ;;
    esac
done

if [ $# -lt 1 ]; then
    echo "usage: $0 [-m commit_message] command [args...]" > /dev/stderr
    exit 1
fi

if [ -z "$ignore_new" ]; then
    status=$(git status --porcelain)
    if [ -n "${status}" ]; then
        echo 'Git repository is dirty' > /dev/stderr
        exit 1
    fi
fi

script="$1"

"${@}"
ret=$?
if [ $ret -ne 0 ]; then
    exit $ret
fi

if [ -n "$ignore_new" ]; then
    status_arg='--untracked-files=no'
fi

status=$(git status --porcelain $status_arg)

if [ -z "${status}" ]; then
    # No changes needed
    exit 0
fi

wrap-and-sort $wns_opt

if [ -z "${commit_message}" ]; then
    commit_message='Automatic update with '"$(basename "${script}")"
fi
git commit -a -m "${commit_message}"

