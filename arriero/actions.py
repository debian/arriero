# -*- coding: utf8 -*-
# Copyright: 2013-2015, Maximiliano Curia <maxy@debian.org>
#
# This program is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the Free
# Software Foundation; either version 2 of the License, or (at your option)
# any later version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
# more details.
#
# You should have received a copy of the GNU General Public License along with
# this program; if not, write to the Free Software Foundation, Inc., 51
# Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

from __future__ import print_function
import argparse
import code
import collections
import glob
import itertools
import logging
import os
import re
import subprocess
import tempfile

import debian.deb822 as deb822

# Own imports
from . import util
from .constants import ERROR, IGNORE, OK
# shell constants
from .constants import (NO, ON_ERROR, ALWAYS)
from .errors import ArrieroError, ActionError
from .version import Version


class Action(object):

    '''Abstract class that all actions should subclass.'''

    def __init__(self, arriero):
        '''Basic constructor for the class that calls the necessary functions.
           It follows the Template Pattern.
        '''
        self._arriero = arriero
        self._process_options()

    @property
    def config(self):
        return self._arriero.config

    def _get_packages(self, names):
        '''Expands the groups in names, returning the package names.'''
        packages = util.OrderedSet()
        for name in names:
            packages_names = self.config.get_members(name)
            logging.debug('name %s got expanded to %s', name, packages_names)
            if packages_names:
                packages.extend(packages_names)
        return packages

    @classmethod
    def add_options(cls, arriero, parser):
        '''Add arguments/config options'''

    def _process_options(self):
        '''Process the options after they were parsed.'''

    def run(self):
        '''Execute this action's main goal. Returns True if successful.'''

    def print_status(self):
        '''Print a status report of the run.'''

    # Methods related to parsing arguments

    @classmethod
    def _add_option_all(cls, arriero, parser):
        parser.add_argument('-a', '--all', action='store_true')

    @classmethod
    def _add_option_suite(cls, arriero, parser):
        parser.add_argument('--suite', default=None, dest='requested_suite')

    # names
    @classmethod
    def _add_option_names(cls, arriero, parser, strict=True):

        class NameChoices(object):

            def __init__(self, names):
                self.names = names

            def __contains__(self, name):
                if name in self.names:
                    return True
                basename, suite = util.split_basename_suite(name)
                return (suite is not None and
                        basename in self.names and
                        suite in self)

            def __str__(self):
                return "NAME[/SUITE] ..."

            def __iter__(self):
                return iter([str(self)])

        if strict:
            name_choices = NameChoices(set(['']) |
                                       set(arriero.config.list_all()))
        else:
            name_choices = None
        parser.add_argument('names', nargs='*', default='',
                            choices=name_choices)

    def _process_option_names_no_name(self, requested_suite=None):
        # Try to find out if we are standing on a package dir
        try:
            cmd = ['git', 'rev-parse', '--show-toplevel']
            result = util.log_run(cmd, check=True)
            cwd = result.stdout.rstrip('\n')
        except Exception:
            cwd = os.getcwd()
        matching_path = []
        suiteless_package = None
        for name in self.config.members:
            package = self._arriero.get_package(name)
            if package.path == cwd:
                matching_path.append(package)
                basename, suite = util.split_basename_suite(name)
                if requested_suite is not None:
                    if requested_suite == (suite or ''):
                        return package.name
                if suite is None:
                    suiteless_package = package
        for package in matching_path:
            if package.branch == package.debian_branch:
                return package.name
        return None if suiteless_package is None else suiteless_package.name

    def _process_option_names(self):
        requested_suite = self.config.get('requested_suite')
        # Make sure the requested suite is valid
        if requested_suite:
            r = self.config.get_members(requested_suite)
            if r is None:
                raise argparse.ArgumentError(None,
                                             'Invalid suite name: {}.'.format(
                                                 requested_suite))

        if self.config.get('all'):
            self.names = self.config.members
        else:
            names = self.config.get('names')
            self.names = []
            for name in names or {''}:
                if not name:
                    name = self._process_option_names_no_name(requested_suite)
                if requested_suite:
                    name = util.join_basename_suite(name, requested_suite)
                if name:
                    self.names.append(name)
        if not self.names:
            raise argparse.ArgumentError(None, 'No package names received.')
        logging.debug('names options expanded to: %s', self.names)

    @classmethod
    def _add_boolean_argument(cls, parser, name, default=False, dest=None):
        '''Helper to add options --name --no-name and set the default'''
        if dest is None:
            dest = name.replace('-', '_')
        parser.add_argument('--{}'.format(name), action='store_true',
                            dest=dest)
        parser.add_argument('--no-{}'.format(name),
                            action='store_false', dest=dest)
        parser.set_defaults(**{dest: default})

    @classmethod
    def _add_option_ignore_branch(cls, arriero, parser):
        cls._add_boolean_argument(parser, 'ignore-branch', default=None)

    @classmethod
    def _add_option_ignore_new(cls, arriero, parser):
        cls._add_boolean_argument(parser, 'ignore-new', default=None)

    @classmethod
    def _add_list_argument(cls, parser, name, default=None, dest=None):
        '''Helper to add list options

        This also adds the --name+="value" --name-="value" options.
        '''
        if dest is None:
            dest = name.replace('-', '_')
        parser.add_argument('--{}'.format(name), default=default, dest=dest)
        parser.add_argument('--{}+'.format(name), default=None,
                            dest='{}+'.format(dest))
        parser.add_argument('--{}-'.format(name), default=None,
                            dest='{}-'.format(dest))


class ActionPackages(Action):

    available_sorts = set(('raw', 'alpha', 'build', 'layer'))

    def _get_name(self):
        return self.__class__.__name__

    @classmethod
    def add_options(cls, arriero, parser):
        super().add_options(arriero, parser)
        cls._add_option_suite(arriero, parser)
        cls._add_option_all(arriero, parser)
        cls._add_option_names(arriero, parser)
        cls._add_option_ignore_branch(arriero, parser)
        cls._add_option_ignore_new(arriero, parser)

    def _process_options(self):
        self._process_option_names()

    def _action_wrapper(self, name, known):

        self._arriero.switch_log(to=name)
        logging.info('%s: executing %s action.',
                     name, self._get_name())
        package = self._arriero.get_package(name)
        try:
            if hasattr(self, '_package_action_fields'):
                status = self._package_action_fields(package, known)
            else:
                status = self._package_action(package)
        except Exception as e:
            status = ERROR
            self._arriero.switch_log()
            logging.error('%s: action %s FAILED with %s',
                          name, self._get_name(), e)
        else:
            if status == ERROR:
                self._arriero.switch_log()
                logging.error('%s: action %s FAILED',
                              name, self._get_name())
        return status

    def run(self):
        '''Method for iterating packages and applying a function.'''
        sort = self.config.get('sort')
        if sort == 'layer':
            return self.layer_run()

        packages = self._sorted(self._get_packages(self.names))
        self._results = collections.defaultdict(set)

        for i, package_name in enumerate(packages):
            known = {'i': i}
            status = self._action_wrapper(package_name, known)
            self._results[status].add(package_name)
        self._arriero.switch_log()

        if self._results[ERROR]:
            return False
        return True

    def layer_run(self):
        def _get_dependencies(package):
            return package.build_depends

        layer = 0
        i = 0
        package_names = self._get_packages(self.names)
        self._results = collections.defaultdict(set)
        graph = self._arriero.prepare_sort_by_depends_graph(
            package_names=package_names,
            get_dependencies=_get_dependencies,
            layer=True)
        skip = set()

        while graph.ready:
            self._layer_action(layer, graph.ready)
            for package_name in graph.sort_generator(skip=skip):
                skip.add(package_name)
                known = {'i': i, 'layer': layer}
                status = self._action_wrapper(package_name, known)
                self._results[status].add(package_name)
                i += 1
            current_layer = skip - self._results[ERROR]
            while current_layer:
                name = current_layer.pop()
                skip.remove(name)
                graph.done(name)
            layer += 1
        self._arriero.switch_log()

        self._results[IGNORE] = ((package_names - self._results[OK]) -
                                 self._results[ERROR])

        if self._results[ERROR]:
            return False
        return True

    def print_status(self):
        log_files = [
            util.AttrDict(
                level=ERROR, filename='error', f=None,
                log=lambda p: logging.error('%s: failed', p)),
            util.AttrDict(
                level=IGNORE, filename='ignore', f=None,
                log=lambda p: logging.info('%s: ignored', p)),
            util.AttrDict(
                level=OK, filename='success', f=None,
                log=lambda p: None),
        ]
        logdir = self._arriero.logdir
        for item in log_files:
            if logdir:
                item.f = open(os.path.join(logdir, item.filename), 'w')
        for item in log_files:
            for package in self._results[item.level]:
                if item.f:
                    item.f.write('{}\n'.format(package))
                item.log(package)
            if item.f:
                item.f.close()

    @classmethod
    def _add_option_sort(cls, arriero, parser):
        parser.add_argument('-s', '--sort', choices=cls.available_sorts,
                            default='raw')

    # Sort methods
    def raw_sort(self, packages):
        return packages

    def alpha_sort(self, packages):
        return sorted(packages)

    def build_sort(self, packages, layer=False):
        def _get_dependencies(package):
            return package.build_depends

        return self._arriero.sort_by_depends(packages,
                                             get_dependencies=_get_dependencies,
                                             layer=layer)

    def layer_sort(self, packages):
        return self.build_sort(packages, layer=True)

    def _sorted(self, packages):
        sort = self.config.get('sort')
        if not sort:
            return packages
        method_name = sort + '_sort'
        method = getattr(self, method_name)
        return method(packages)

    def _layer_action(self, layer, items):
        '''To override.'''

    def _package_action(self, package):
        '''To override.'''

    # If present it would call:
    # def _package_action_i(self, package, i):
    #    '''To override.'''


class ActionFields(ActionPackages):

    '''Common class for List and Exec'''

    available_fields = set([
        'basedir',
        'basename',
        'branch',
        'build_file',
        'build_depends',
        'builder_distribution',
        'changes_file',
        'debian_branch',
        'dist',
        'distribution',
        'dsc_file',
        'export_dir',
        'has_symbols',
        'i',
        'is_dfsg',
        'is_native',
        'is_merged',
        'last_changelog_distribution',
        'last_changelog_version',
        'layer',
        'name',
        'package',
        'parents',
        'path',
        'runtime_depends',
        'runtime_recommends',
        'scripts_dir',
        'source_name',
        'source_changes_file',
        'tarball_dir',
        'target_distribution',
        'test_depends',
        'upstream_branch',
        'upstream_version',
        'upstream_vcs_git',
        'urgency',
        'vcs_git',
        'vendor',
        'version',
        'epochless_version',
        'version_at_distribution',
    ])
    aliases_fields = {
        'dist': 'distribution',
        'package': 'name',
    }

    @classmethod
    def add_options(cls, arriero, parser):
        super().add_options(arriero, parser)
        parser.add_argument('-f', '--fields', default='name')
        cls._add_option_sort(arriero, parser)

    def _process_options(self, *formats):
        super()._process_options()
        self.fields = util.split(self.config.get('fields'))
        self.field_names = {}

        required_fields = []
        for format_string in formats:
            if not format_string:
                continue
            _, f = util.expansions_needed(format_string)
            required_fields.extend(f)

        for field_name in itertools.chain(self.fields, required_fields):
            if field_name not in self.available_fields:
                raise ActionError(
                    'action %s: Field %s not known' % (
                        self._get_name(),
                        field_name
                    )
                )
            self.field_names[field_name] = None

    def _get_field(self, package, field):
        if not hasattr(package, field):
            return package.get(field)
        obj = getattr(package, field)
        if hasattr(obj, '__call__'):
            result = obj()
        else:
            result = obj
        return result

    @staticmethod
    def _stringify(value):
        return (value if isinstance(value, str) else
                '' if value is None else
                str(tuple(value)) if (
                    isinstance(value, (set,
                                       deb822.OrderedSet,
                                       collections.Set))) else
                str(value))

    def _resolve_aliases(self, field, visited=None):
        if field not in self.aliases_fields:
            return field
        if not visited:
            visited = set()
        if field in visited:
            raise ActionError('action %s: Invalid alias %s' %
                              (self.__class__.__name__, field))
        visited.add(field)
        return self._resolve_aliases(self.aliases_fields[field], visited)

    def _get_field_values(self, package, known=None):
        values = []
        by_name = {}
        for field in self.field_names:
            lookup = self._resolve_aliases(field)
            if known and field in known:
                raw_value = known[field]
            else:
                raw_value = self._get_field(package, lookup)
            field_value = self._stringify(raw_value)
            by_name[field] = field_value

        for field in self.fields:
            values.append(by_name[field])

        return values, by_name


class List(ActionFields):

    '''Query the available packages with formatting.'''

    @classmethod
    def add_options(cls, arriero, parser):
        super().add_options(arriero, parser)
        parser.add_argument('-F', '--format', default=None,
                            dest='output_format')
        parser.add_argument('--field-separator', default='\t')
        parser.add_argument('--separator', default=None)
        parser.add_argument('-e', '--include-empty-results',
                            action='store_true')

    def _process_options(self):
        self.output_format = self.config.get('output_format', raw=True)
        super()._process_options(self.output_format)
        self.include_empty_results = self.config.get('include_empty_results')
        self.separator = self.config.get('separator', raw=True)
        self.field_separator = self.config.get('field_separator', raw=True)

    def _is_empty(self, iterable):
        '''Returns True if the iterables has all empty elements.'''
        if not iterable:
            return True
        for value in iterable:
            if value:
                return False
        return True

    def _package_action_fields(self, package, known):
        values, by_name = self._get_field_values(package, known)

        if not self.include_empty_results and self._is_empty(values):
            return OK

        kw = {}
        if self.output_format:
            args = [self.output_format.format(*values, **by_name)]
        else:
            args = [self.field_separator.join(values)]

        if self.separator is not None:
            kw['end'] = ''
            kw['sep'] = self.separator
            kw['flush'] = True
            if 'i' in known and known['i'] >= 1:
                args.insert(0, '')

        print(*args, **kw)

        return OK


class Exec(ActionFields):

    '''Run a command on each package.'''

    @classmethod
    def add_options(cls, arriero, parser):
        super().add_options(arriero, parser)
        parser.add_argument('-x', '--script', action='append')
        parser.add_argument('--no-env',
                            action='store_false',
                            dest='env')
        parser.add_argument('--no-chdir',
                            action='store_false',
                            dest='chdir')

    def _process_options(self):
        self._scripts = self.config.get('script', raw=True)
        super()._process_options(*self._scripts)

    def _package_action_fields(self, package, known):
        status = OK
        kwargs = {'interactive': True, 'shell': True}

        if self.config.get('chdir'):
            kwargs['cwd'] = package.path

        values, by_name = self._get_field_values(package, known)

        scripts_dir = self.config.get('scripts_dir')
        path = os.environ['PATH'] + ':{}'.format(scripts_dir)
        kwargs['env'] = dict(os.environ, PATH=path)

        if self.config.get('env'):
            kwargs['env'] = dict(kwargs['env'], **by_name)

        for script in self._scripts:
            script_formatted = script.format(*values, **by_name)

            try:
                util.log_check_call(script_formatted, **kwargs)
            except (ArrieroError, subprocess.CalledProcessError) as e:
                logging.error('%s: %s', package.name, e)
                status = ERROR
                break
        return status


class Clone(Action):

    '''Clone upstream repositories.'''

    @classmethod
    def add_options(cls, arriero, parser):
        cls._add_option_all(arriero, parser)
        cls._add_option_names(arriero, parser, strict=False)
        parser.add_argument('--basedir', default=None)
        parser.add_argument('--upstream-branch', default=None)
        parser.add_argument('--debian-branch', default=None)
        parser.add_argument('--vcs-git', default=None)
        parser.add_argument('--upstream-vcs-git', default=None)

    def _process_options(self):
        self._process_option_names()

        # Split the names into URLs and packages.
        self._packages = set()
        self._urls = set()
        for name in self.config.get('names'):
            if ':' in name:
                self._urls.add(name)
            else:
                self._packages.add(self._arriero.get_package(name))

    def run(self):
        self._not_ok = set()
        for url in self._urls:
            try:
                if not self.url_clone(url):
                    self._not_ok.add(url)
            except Exception as e:
                logging.error('%s: Failed to clone.', url)
                logging.error('%s: %s', url, e)
                self._not_ok.add(url)

        for package in self._packages:
            self._arriero.switch_log(to=package.name)
            try:
                if not self.package_clone(package):
                    self._not_ok.add(package.name)
            except Exception as e:
                logging.error('%s: Failed to clone.', package.name)
                logging.error('%s: %s', package.name, e)
                self._not_ok.add(package.name)

        # Run status
        if self._not_ok:
            return False
        return True

    def get_remote_heads(self, url):
        heads = set()
        cmd = ['git', 'ls-remote', '--heads', url]
        p = subprocess.Popen(cmd, universal_newlines=True,
                             stdout=subprocess.PIPE)
        for line in p.stdout:
            m = re.search(r'\srefs/heads/(.*)$', line)
            if m:
                heads.add(m.group(1))
        return heads

    def guess_branches(self, url):
        '''Use git ls-remote to check which branches are there.'''
        heads = self.get_remote_heads(url)
        upstream_branch = 'upstream'
        debian_branch = 'master'
        # Review this: Lucky guess?
        if 'debian' in heads:
            debian_branch = 'debian'
            if 'upstream' not in heads:
                if 'master' in heads:
                    upstream_branch = 'master'
        elif 'sid' in heads:
            debian_branch = 'sid'
        elif 'unstable' in heads:
            debian_branch = 'unstable'
        pristine_tar = False
        if 'pristine-tar' in heads:
            pristine_tar = True

        return debian_branch, upstream_branch, pristine_tar

    def url_clone(self, url):
        '''Clone a package from the provided url.'''
        # TODO: gbp clone also handles "vcsgit:" uris

        # Check if this URL is already configured
        for package_name in self.config.members:
            package = self._arriero.get_package(package_name)
            if package.vcs_git == url:
                logging.warning(
                    'The URL %s is already configured by package %s.',
                    package.vcs_git, package.name)
                logging.warning('Switching to cloning from configuration file.')
                self._packages.add(package)
                return True

        # Get basedir for this package
        basedir = self.config.get('basedir')

        # Guess destdir for this package
        name = os.path.basename(url)
        if name.endswith('.git'):
            name = name[:-4]
        destdir = os.path.join(basedir, name)

        # Guess the branches
        debian_branch, upstream_branch, pristine_tar = self.guess_branches(url)

        # Get upstream_vcs_git for this package
        upstream_remote = self.config.get('upstream_vcs_git')

        self.clone(basedir, destdir, url, debian_branch, upstream_branch,
                   pristine_tar, upstream_remote)

        # Obtain package name from control file
        destdir = os.path.expanduser(destdir)
        control_filepath = os.path.join(destdir, 'debian', 'control')
        if not os.path.exists(control_filepath):
            logging.error('Unable to find debian/control while cloning %s', url)
            return False
        control_file = open(control_filepath)
        # Deb822 will parse just the first paragraph, which is ok.
        control = deb822.Deb822(control_file)
        package_name = control['Source']

        if not self._arriero.add_new_package(package_name, url, destdir,
                                             debian_branch, upstream_branch,
                                             pristine_tar):
            logging.error('Clone successful for package not in configuration. '
                          'You will not be able to use arriero with it.')
            return False

        package = self._arriero.get_package(package_name)
        return package.fetch_upstream()

    # TODO: this method should probably be in the package and not here
    def clone(self, basedir, destdir, url, debian_branch, upstream_branch,
              pristine_tar, upstream_remote):
        '''Verify the directories for the clone, and clone.'''

        basedir = os.path.expanduser(basedir)
        destdir = os.path.expanduser(destdir)
        util.ensure_path(basedir)
        logging.debug('basedir: %s', basedir)
        logging.debug('destdir: %s', destdir)

        if os.path.exists(destdir):
            logging.error('Cloning %s, directory already exists: %s',
                          url, destdir)
            return False
        dirname, basename = os.path.split(destdir)

        cmd = ['gbp', 'clone']
        if debian_branch:
            cmd.append('--debian-branch=%s' % debian_branch)
        if upstream_branch:
            cmd.append('--upstream-branch=%s' % upstream_branch)
        if pristine_tar:
            cmd.append('--pristine-tar')
        else:
            cmd.append('--no-pristine-tar')
        cmd.append(url)
        cmd.append(basename)

        util.log_check_call(cmd, interactive=True, cwd=dirname)
        logging.info('Successfully cloned %s', url)

        if upstream_remote:
            cmd = ['git', 'remote', 'add', 'upstream', upstream_remote]
            util.log_check_call(cmd, interactive=True, cwd=destdir)
            cmd = ['git', 'fetch', 'upstream']
            util.log_check_call(cmd, interactive=True, cwd=destdir)

        return True

    def package_clone(self, package):
        """Clone a package that is already in the config file."""

        # TODO: shouldn't we check if this returned true or false?
        self.clone(package.basedir, package.path, package.vcs_git,
                   package.debian_branch, package.upstream_branch,
                   package.pristine_tar,
                   package.upstream_vcs_git)

        if package.name not in self.config.list_all():
            success = self._arriero.add_new_package(
                package.name, package.vcs_git, package.path,
                package.debian_branch, package.upstream_branch, package.pristine_tar)

            if not success:
                logging.error('Clone successful for package not in configuration. '
                              'You will not be able to use arriero with it.')

        return package.fetch_upstream()


class Test(ActionPackages):

    '''Merge and test the received packages.'''
    @classmethod
    def add_options(cls, arriero, parser):
        super().add_options(arriero, parser)
        parser.add_argument('-D', '--distribution', '--dist',
                            default=None, dest='target_distribution')
        parser.add_argument('--builder-distribution',
                            default=None, dest='builder_distribution')
        parser.add_argument('-A', '--architecture', '--arch',
                            default=None)
        parser.add_argument('--extra-repository', action='append')
        parser.add_argument('--tester', default=None)
        parser.add_argument('--run-autopkgtest', default=None)
        parser.add_argument('--run-lintian', default=None)
        parser.add_argument('--run-piuparts', default=None)
        parser.add_argument('--lintian-options', default=None)
        parser.add_argument('--shell', dest='test_shell',
                            action='store_const', const=ALWAYS)
        parser.add_argument('--shell-fail', '-s', dest='test_shell',
                            action='store_const', const=ON_ERROR)
        parser.set_defaults(test_shell=NO)

    def _package_action(self, package):
        status = OK
        try:
            package.test(shell=package.get('test_shell'))
        except Exception as e:
            logging.error('%s: Test failed', package.name)
            logging.error('%s: %s', package.name, e)
            status = ERROR
        return status


class Build(ActionPackages):

    '''Merge and compile the received packages.'''
    @classmethod
    def add_options(cls, arriero, parser):
        super().add_options(arriero, parser)
        parser.add_argument('-D', '--distribution', '--dist',
                            default=None, dest='target_distribution')
        parser.add_argument('--builder-distribution',
                            default=None, dest='builder_distribution')
        parser.add_argument('-A', '--architecture', '--arch',
                            default=None)
        parser.add_argument('-U', action='store_true', dest='upload_on_build')
        parser.add_argument('--host', dest='upload_host', default=None)
        cls._add_boolean_argument(parser, 'sign-upload', default=None)
        parser.add_argument('-S', '--source-only', action='store_true')
        parser.add_argument('-B', '--binary-only', action='store_true')
        cls._add_boolean_argument(parser, 'arch-all', default=True)
        cls._add_boolean_argument(parser, 'arch-any', default=True)
        parser.add_argument('--force-orig-source', action='store_true')
        parser.add_argument('-o', '--builder-options', action='append')
        parser.add_argument('--extra-repository', action='append')
        parser.add_argument('--builder', default=None)
        parser.add_argument('--tester', default=None)
        cls._add_list_argument(parser, 'hooks', dest='builder_hooks')
        cls._add_boolean_argument(parser, 'tag', default=None)
        parser.add_argument('--run-autopkgtest', default=None)
        parser.add_argument('--run-lintian', default=None)
        parser.add_argument('--run-piuparts', default=None)
        parser.add_argument('--lintian-options', default=None)
        cls._add_boolean_argument(parser, 'push', default=None,
                                  dest='push_on_build')
        cls._add_boolean_argument(parser, 'debian-tags', default=None)
        # TODO: add an exec option
        parser.add_argument('--layer-action', choices=('', 'shell', 'python'),
                            default='')

    def _package_action(self, package):

        # TODO: why is build catching the exception?
        package.build()
        package.tag()
        if package.get('upload_on_build'):
            if package.get('source_only'):
                package.upload(changes_file=package.source_changes_file)
            else:
                package.upload()
        if package.get('push_on_build') or \
           (package.get('upload_on_build') and package.get('push_on_upload')):
            package.push()

        return OK

    def _layer_action(self, layer, items):
        action = self.config.get('layer_action')
        if action == 'shell':
            cmd = [os.environ['SHELL']]
            env = dict(os.environ, LAYER=str(layer), ITEMS=util.join(items))
            return util.log_call(cmd, check=False, env=env)
        if action == 'python':
            return code.interact(local=locals())

    def run(self):
        return self.layer_run()


class Upload(ActionPackages):

    @classmethod
    def add_options(cls, arriero, parser):
        super().add_options(arriero, parser)
        parser.add_argument('-S', '--source-only', action='store_true')
        parser.add_argument('--host', dest='upload_host', default=None)
        parser.add_argument('-f', '--force', action='store_true')
        cls._add_boolean_argument(parser, 'tag', default=None)
        cls._add_boolean_argument(parser, 'sign-upload', default=None)
        cls._add_boolean_argument(parser, 'push', default=None,
                                  dest='push_on_upload')
        cls._add_boolean_argument(parser, 'debian-tags', default=None)

    def _package_action(self, package):
        try:
            package.tag()
            if package.get('source_only'):
                package.upload(changes_file=package.source_changes_file)
            else:
                package.upload()
            if package.get('push_on_upload'):
                package.push()
        except Exception as e:
            logging.error('%s: Failed to upload.', package.name)
            logging.error('%s: %s', package.name, e)
            return ERROR


class Pull(ActionPackages):

    def _package_action(self, package):
        status = OK

        try:
            package.pull()
            logging.info('%s: Successfully pulled.', package.name)
        except ArrieroError as e:
            logging.error('%s: Failed to pull.', package.name)
            logging.error('%s: %s', package.name, e)
            status = ERROR

        return status


class Push(ActionPackages):

    @classmethod
    def add_options(cls, arriero, parser):
        super().add_options(arriero, parser)
        cls._add_boolean_argument(parser, 'debian-tags', default=None)

    def _package_action(self, package):
        status = OK
        if not package.push():
            status = ERROR
        return status


class Release(ActionPackages):

    @classmethod
    def add_options(cls, arriero, parser):
        super().add_options(arriero, parser)
        parser.add_argument('-D', '--distribution', '--dist',
                            default=None, dest='target_distribution')
        parser.add_argument('-P', '--pre-release', action='store_true')
        parser.add_argument('--bpo', action='store_true')

    def _package_action(self, package):

        return package.release()


class Update(ActionPackages):

    @classmethod
    def add_options(cls, arriero, parser):
        super().add_options(arriero, parser)
        parser.add_argument('-V', '--download-version', default=None)

    def _package_action(self, package):
        status = OK
        try:
            package.new_upstream_release()
        except (ArrieroError,
                subprocess.CalledProcessError) as e:
            logging.error('%s: Failed to get new upstream release.',
                          package.name)
            logging.error('%s: %s', package.name, e)
            status = ERROR
        return status


class Status(ActionPackages):

    def _package_action(self, package):
        print('\n'.join(package.get_status()))


class PrepareOverlay(ActionPackages):

    @classmethod
    def add_options(cls, arriero, parser):
        super().add_options(arriero, parser)
        parser.add_argument('-b', '--branch', default=None,
                            dest='overlay_branch')

    def _package_action(self, package):
        status = OK
        try:
            package.prepare_overlay(
                overlay_branch=package.get('overlay_branch'))
        except Exception as e:
            logging.error('%s: failure while preparing overlay', package.name)
            logging.error('%s: %s', package.name, e)
            status = ERROR
        return status


class FetchUpstream(ActionPackages):

    def _package_action(self, package):
        status = OK
        try:
            package.fetch_upstream()
        except Exception as e:
            logging.error('%s: unable to fetch upstream release', package.name)
            logging.error('%s: %s', package.name, e)
            status = ERROR
        return status


class CheckIfChanged(ActionPackages):

    def _package_action(self, package):
        status = OK
        if package.is_native():
            return status
        current = previous = package.upstream_version
        for block in package.changelog:
            version = Version(str(block.version))
            if version.upstream_version != current:
                previous = version.upstream_version
                break
        if current == previous:
            return status
        changes = package.upstream_changes(old_version=previous)

        if changes:
            msg = 'changes since {}'.format(previous)
        else:
            msg = '{} = {}'.format(current, previous)
        print('{}: {}'.format(package.name, msg))

        return status


class UpdateSymbols(ActionPackages):

    @classmethod
    def add_options(cls, arriero, parser):
        super().add_options(arriero, parser)
        parser.add_argument('-D', '--distribution', '--dist',
                            default=None, dest='target_distribution')
        parser.add_argument('--from-build-log', action='store_true')
        parser.add_argument('--from-buildds-logs', action='store_true')

    @staticmethod
    def _has_symbols_changes(package, since):
        release_tag = package.tag_template('debian', version=since)
        repo = package.repo
        repo_tag = repo.tags[release_tag]
        return bool(repo.index.diff(repo_tag.commit, 'debian/*.symbols'))

    @staticmethod
    def _symbols_helper(package, version, files):
        upstream_version, _ = str(version).split('-')

        cmd = ['pkgkde-symbolshelper', 'batchpatch', '-v', upstream_version]
        cmd.extend(files)
        try:
            util.log_check_call(cmd, cwd=package.path)
        except subprocess.CalledProcessError:
            return None

        # Check index with working dir
        diffs = package.repo.index.diff(None)

        return [diff.a_blob.path for diff in diffs]

    @staticmethod
    def _process_buildd_logs(package, version):
        cmd = ['getbuildlog', package.source_name, version]
        util.log_check_call(cmd, cwd=package.export_dir)
        filename_glob = '{}_{}_*.log*'.format(package.source_name, version)
        downloaded_glob = os.path.join(package.export_dir, filename_glob)
        downloaded_files = glob.glob(downloaded_glob)

        return UpdateSymbols._symbols_helper(package, version, downloaded_files)

    @staticmethod
    def _process_build_log(package):
        files = [package.build_file]
        return UpdateSymbols._symbols_helper(package, package.version, files)

    @staticmethod
    def _has_missing_symbols(package, changes):
        missing_re = re.compile(r'^\s*#(DEPRECATED|MISSING)')
        for filename in changes:
            fullpath = os.path.join(package.path, filename)
            f = open(fullpath)
            for line in f:
                if missing_re.match(line):
                    return True
        return False

    @staticmethod
    def _get_symbols_mtime(package):
        return max(os.path.getmtime(f) for f in
                   package.symbols_files(ignore_branch=True))

    def _buildds_logs_action(self, package):
        status = OK

        # Obtain last distribution
        changelog_dist = package.get('target_distribution')
        if not changelog_dist or changelog_dist == 'UNRELEASED':
            changelog_dist = package.last_changelog_distribution
        if not changelog_dist:
            logging.info(
                '%s: no previous upload (dist), ignoring.',
                package.name)
            return IGNORE

        # Obtain last uploaded version
        version = util.version_at_distribution(package.source_name,
                                               changelog_dist)
        if not version:
            logging.info(
                '%s: no previous upload, ignoring.', package.name)
            return IGNORE

        # Check if symbols have changed since last upload
        if self._has_symbols_changes(package, since=version):
            logging.info(
                '%s: there were changes since last upload, ignoring.',
                package.name)
            return IGNORE

        # Obtain the buildds logs and
        # Update the symbols with the buildds results
        changes = self._process_buildd_logs(package, str(version))
        if not changes:
            logging.info('%s: no changes needed.', package.name)
            return status

        # Check for missing symbols, let the user fix those
        if self._has_missing_symbols(package, changes):
            logging.error('%s: missing symbols', package.name)
            return ERROR

        # Commit the symbols changes
        package.commit(
            msg='Update symbols files from buildds logs ({}).'.format(version),
            files=changes)

        return status

    @staticmethod
    def _build_log_action(package):
        status = OK

        # Obtain build log
        if not package.build_file:
            logging.info('%s: no build file, ignoring.', package.name)
            return IGNORE

        build_mtime = os.path.getmtime(package.build_file)

        # Obtain newer mtime of the symbols files
        symbols_mtime = UpdateSymbols._get_symbols_mtime(package)

        # Skip if symbols mtime is newer than the build log mtime
        if symbols_mtime > build_mtime:
            logging.info('%s: build is previous to the last symbols change'
                         ', ignoring', package.name)
            return IGNORE

        # Process build log
        changes = UpdateSymbols._process_build_log(package)
        if not changes:
            logging.info('%s: no changes needed.', package.name)
            return status

        # Check for missing symbols, let the user fix those
        if UpdateSymbols._has_missing_symbols(package, changes):
            logging.error('%s: missing symbols', package.name)
            return ERROR

        # Commit the symbols changes
        package.commit(msg='Update symbols files.', files=changes)

        return status

    def _package_action(self, package):
        status = OK

        if (not package.get('ignore_new') and
                package.repo.is_dirty(untracked_files=True)):
            logging.warning(
                '%s: branch %s has uncommitted changes.',
                package.name, package.repo.active_branch.name)
            return ERROR

        if not package.has_symbols():
            logging.info(
                '%s: no symbols files, ignoring.', package.name)
            return IGNORE

        buildds = self.config.get('from_buildds_logs')
        build = self.config.get('from_build_log')
        if buildds or build:
            if buildds:
                status = self._buildds_logs_action(package)
            if status != ERROR and build:
                status = self._build_log_action(package)
        else:
            if not package.build_file:
                return self._buildds_logs_action(package)
            return self._build_log_action(package)


class ExternalUploads(ActionPackages):

    @classmethod
    def add_options(cls, arriero, parser):
        super().add_options(arriero, parser)
        cls._add_boolean_argument(parser, 'import', dest='do_import')

    def _check_external_uploads(self, package):

        def drop_binnmu_part(version):
            m = re.match('^(.*)\+b[0-9]+$', version)
            if m:
                return m.group(1)
            return version

        status = OK
        archive_versions = util.rmadison(package.source_name)
        pending = collections.defaultdict(set)
        for dist in archive_versions:
            for version in dist:
                version = drop_binnmu_part(version)
                pending[version].add(dist)

        for block in package.changelog:
            if block.version in pending:
                pending.pop(block.version)
        for version, dists in pending.items():
            print('{}: missing version {} available in {}'.format(
                package.name, version, dists))
        if pending:
            status = ERROR
        return pending, status

    def _import_external_uploads_get_import_dsc_cmd(self, package, tmpdir):
        # gbp import-dsc
        dsc_file = glob.glob('{}/*.dsc'.format(tmpdir))[0]
        cmd = ['gbp', 'import-dsc', dsc_file]
        if package.debian_branch:
            cmd.append('--debian-branch=%s' % package.debian_branch)
        if package.upstream_branch:
            cmd.append('--upstream-branch=%s' % package.upstream_branch)
        if package.pristine_tar:
            cmd.append('--pristine-tar')
            if package.filter_orig:
                cmd.append('--filter-pristine-tar')
                for pattern in package.filter_orig:
                    cmd.append('--filter=%s' % pattern)
        else:
            cmd.append('--no-pristine-tar')
        return cmd

    def _import_external_uploads(self, package):

        pending, status = self._check_external_uploads(package)
        for version, dists in pending.items():
            # Do something
            if 'new' in dists:
                logging.info('%s: ignoring version %s in new',
                             package.name, version)
                dists.remove('new')
                status = IGNORE
            incoming = set()
            for dist in dists:
                if dist.startswith('buildd-'):
                    incoming.add(dist)
            if dists - incoming:
                # In the archive, using debsnap
                with tempfile.TemporaryDirectory(prefix=package.name) as tmpdir:
                    logging.warn('%s: gbp import-dsc always merges the '
                                 'upstream source into the debian branch',
                                 package.name)
                    cmd = ['debsnap', '--destdir={}'.format(tmpdir),
                           '--force', package.source_name, version]
                    util.log_check_call(cmd)
                    cmd = self._import_external_uploads_get_import_dsc_cmd(
                        package, tmpdir)
                    util.log_check_call(cmd)
                status = OK
            elif incoming:
                logging.info('%s: ignoring version %s in incoming',
                             package.name, version)
                status = IGNORE
        return status

    def _package_action(self, package):

        status = OK
        try:
            package.pull()
        except Exception:
            return ERROR

        if not self.config.get('do_import'):
            versions, status = self._check_external_uploads(package)
        else:
            status = self._import_external_uploads(package)
        return status


class InternalDependencies(ActionPackages):

    @classmethod
    def add_options(cls, arriero, parser):
        super().add_options(arriero, parser)
        parser.add_argument('--between-selected', action='store_true')
        cls._add_option_sort(arriero, parser)

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.binaries = {}

        if self.config.get('between_selected'):
            package_names = self._get_packages(self.names)
        else:
            package_names = self.config.members

        for package_name in package_names:
            package = self._arriero.get_package(package_name)
            if not os.path.isdir(package.path):
                # Skip not cloned packages
                continue
            binaries = package.get_packages()
            for binary_name in binaries:
                self.binaries[binary_name] = package_name

    def _package_action(self, package):
        internal_dep = package.internal_dependencies(self.binaries)
        internal_dep = {
            key: {'due_to': value} for (key, value) in internal_dep.items()
        }

        for dep, value in internal_dep.items():
            value['groups'] = \
                sorted(self.config.list_parents(dep))

        nicer = sorted(internal_dep.items(),
                       key=lambda x: (x[1]['groups'], x[0]))

        current_group = None
        groups = []
        for item in nicer:
            if item[1]['groups'] != current_group:
                current_group = item[1]['groups']
                groups.append([])
            groups[-1].append(item[0])
            print('# {:32} : due to {} ({})'.format(
                item[0], ', '.join(item[1]['due_to']),
                util.join(item[1]['groups'])))
        print('; {}'.format(
            util.join(sorted(self.config.list_parents(package.name)))
        ))
        print('[{}]'.format(package.name))
        if groups:
            print('depends: ' + '\n         '.join(util.join(g) for g in groups))
        # print()
        # for item in nicer:
        #     print('{}'.format(item[0]))

    def _layer_action(self, layer, items):
        print(';;;\n; Layer {}\n;;;'.format(layer))


# Dictionary containing the available actions in this module and some help
# about what they are for.

AVAILABLE_ACTIONS = {
    'status': (Status, 'Show the status of the package/s'),
    'update': (Update, 'Get the new upstream release of the package/s'),
    'build': (Build, 'Build the package/s in a pbuilder'),
    'list': (List, 'List package/s with a specific format'),
    'exec': (Exec, 'Run commands for each package/s'),
    'clone': (Clone, 'Obtain the repository for the package/s'),
    'upload': (Upload, 'Upload the package/s'),
    'pull': (Pull, 'Pull new changes to the package/s repositories'),
    'push': (Push, 'Push local changes to the package/s repositories'),
    'release': (Release, 'Change the distribution in the changelog'),
    'test': (Test, 'Run the package/s tests'),
    'overlay': (PrepareOverlay, 'Combine upstream and debian branches'),
    'fetch-upstream': (FetchUpstream, 'Fetch upstream tarball'),
    'check-if-changed': (CheckIfChanged, 'Check changes in the upstream tarball'),
    'update-symbols': (UpdateSymbols, 'Update symbols files with previous build'),
    'external-uploads': (ExternalUploads, 'Break stuff'),
    'internal-dependencies': (InternalDependencies, 'List dependencies between packages'),
}

# vi:expandtab:softtabstop=4:shiftwidth=4:smarttab
