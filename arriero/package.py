#!/usr/bin/env python3
# -*- coding: utf8 -*-
# Copyright: 2013-2014, Maximiliano Curia <maxy@debian.org>
#
# This program is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the Free
# Software Foundation; either version 2 of the License, or (at your option)
# any later version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
# more details.
#
# You should have received a copy of the GNU General Public License along with
# this program; if not, write to the Free Software Foundation, Inc., 51
# Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

import collections
import glob
import logging
import os
import re

import debian.changelog as changelog
import debian.deb822 as deb822
import debian.debian_support as ds
import git

# Own imports
from . import util
from .constants import (OK, IGNORE, ERROR, NO)
from .dch import get_dch
from .errors import (GitBranchError, GitDirty, GitDiverge,
                     GitRemoteNotFound, PackageError, UscanError)
from .uscan import Uscan
from .version import Version


# Precompiled regexps
dfsg_re = re.compile(r'(.*)[+.]dfsg(?:.\d+)?$')


class Package(object):

    class Getter(object):

        '''Provides a mapping face to the internal values

        The sole purpose of this class is to provide a mapping like interface
        to be used from the Configuration class.
        '''

        def __init__(self, package):
            self.package = package

        def __contains__(self, key):
            if not isinstance(key, str):
                return False
            attr_name = '_field_{}'.format(key)
            return hasattr(self.package, attr_name)

        def __getitem__(self, key):
            if self.__contains__(key):
                attr_name = '_field_{}'.format(key)
                return getattr(self.package, attr_name)
            raise KeyError()

    _field_pristine_tar_branch = 'pristine-tar'

    def __init__(self, name, arriero):
        self._field_name = name
        self._field_vcs_git = None
        self._field_path = None
        self._arriero = arriero
        self._field_architecture = self._arriero.architecture
        self._repo = None
        self._changelog = None
        self._control = None
        self._tests_control = None
        self._last_changelog = None

    @property
    def config(self):
        return self._arriero.config

    def get(self, *a, **kw):
        kw.update({'section': self._field_name, 'instance': self})
        return self.config.get(*a, **kw)

    def set(self, option, value):
        return self.config.set(self._field_name, option, value)

    @property
    def parents(self):
        return self.config.list_parents(self._field_name)

    def _getter(self):
        return self.Getter(self)

    @property
    def name(self):
        return self._field_name

    @property
    def basename(self):
        return self._field_basename

    @property
    def _field_basename(self):
        return util.split_basename_suite(self._field_name)[0]

    @property
    def architecture(self):
        return self.get('architecture')

    @property
    def basedir(self):
        return self.get('basedir')

    @property
    def path(self):
        if not self._field_path:
            self._field_path = self.get('path')
        return self._field_path

    @property
    def builder_distribution(self):
        return self.get('builder_distribution')

    @builder_distribution.setter
    def builder_distribution(self, distribution):
        self.set('builder_distribution', distribution)

    @property
    def target_distribution(self):
        return self.get('target_distribution')

    @target_distribution.setter
    def target_distribution(self, distribution):
        self.set('target_distribution', distribution)

    @property
    def upstream_branch(self):
        return self.get('upstream_branch')

    @property
    def debian_branch(self):
        return self.get('debian_branch')

    @property
    def filter_orig(self):
        return self.get('filter_orig')

    @property
    def pristine_tar(self):
        return self.get('pristine_tar')

    @property
    def pristine_tar_branch(self):
        return self._field_pristine_tar_branch

    @property
    def is_merged(self):
        return self.get('is_merged')

    @property
    def _field_vsc_git(self):
        try:
            value = self.source_control.get('Vcs-Git')
            if value is not None:
                self.vcs_git = value
        except PackageError:
            # No control file.
            return None

    @property
    def vcs_git(self):
        return self.get('vcs_git')

    @vcs_git.setter
    def set_vcs_git(self, value):
        return self.set('vcs_git', value)

    @property
    def upstream_vcs_git(self):
        return self.get('upstream_vcs_git')

    def check_path(self):
        if os.path.isdir(self.path):
            return self.path

    @property
    def tarball_dir(self):
        return self.get('tarball_dir')

    @property
    def export_dir(self):
        return self.get('export_dir')

    @property
    def repo(self):
        if not self._repo:
            self._repo = git.Repo(self.path)
        return self._repo

    @property
    def git(self):
        return self.repo.git

    @property
    def _field_branch(self):
        if self.repo.head_is_detached:
            return None
        return self.repo.head.shorthand

    @property
    def branch(self):
        if self.repo.head.is_detached:
            return None
        return self.repo.active_branch.name

    @property
    def changelog(self):
        if not self._changelog:
            self.update_changelog()
        return self._changelog

    def update_changelog(self):
        changelog_filename = os.path.join(self.path, 'debian', 'changelog')
        try:
            with open(changelog_filename) as f:
                self._changelog = changelog.Changelog(f)
        except EnvironmentError:
            raise PackageError('Could not open the changelog file')

        if not len(self._changelog):
            raise PackageError('Changelog file could not be parsed')

    @property
    def control(self):
        if not self._control:
            self._control = self.update_control(
                os.path.join(self.path, 'debian', 'control'))
        return self._control

    @property
    def source_control(self):
        return self.control[0]

    @property
    def binary_controls(self):
        return self.control[1:]

    @property
    def tests_control(self):
        if not self.has_tests():
            return
        if not self._tests_control:
            self._tests_control = self.update_control(
                os.path.join(self.path, 'debian', 'tests', 'control'))
        return self._tests_control

    def update_control(self, filename):
        try:
            with open(filename) as f:
                return list(deb822.Deb822.iter_paragraphs(f))
        except EnvironmentError:
            raise PackageError('Could not open the control file')

    @property
    def source_name(self):
        return self.source_control['Source']

    @property
    def build_file(self):
        build_file = os.path.join(
            self.export_dir,
            '{}_{}_{}.build'.format(
                self.source_name, self.epochless_version, self.architecture)
        )
        if not os.path.exists(build_file):
            return ''
        return build_file

    @property
    def changes_file(self):
        changes_file = os.path.join(
            self.export_dir,
            '{}_{}_{}.changes'.format(
                self.source_name, self.epochless_version, self.architecture)
        )
        if os.path.exists(changes_file):
            return changes_file
        changes_file = os.path.join(
            self.export_dir,
            '{}_{}_{}.changes'.format(
                self.source_name, self.epochless_version, 'all')
        )
        if not os.path.exists(changes_file):
            return ''
        return changes_file

    @property
    def source_changes_file(self):
        changes_file = os.path.join(
            self.export_dir,
            '{}_{}_source.changes'.format(
                self.source_name, self.epochless_version)
        )
        if not os.path.exists(changes_file):
            return ''
        return changes_file

    @property
    def upload_files(self):
        upload_file_glob = os.path.join(
            self.export_dir,
            '{}_{}_*.upload'.format(
                self.source_name, self.epochless_version)
        )
        return glob.glob(upload_file_glob)

    @property
    def dsc_file(self):
        dsc_file = os.path.join(
            self.export_dir,
            '{}_{}.dsc'.format(self.source_name, self.epochless_version)
        )
        if not os.path.exists(dsc_file):
            return ''
        return dsc_file

    @property
    def version(self):
        return Version(self.changelog.version)

    @property
    def _field_version(self):
        return str(self.version)

    @property
    def epochless_version(self):
        epochless_version = self.version.upstream_version
        if self.version.debian_revision:
            epochless_version += '-{}'.format(self.version.debian_revision)
        return Version(epochless_version)

    @property
    def _field_epochless_version(self):
        return str(self.epochless_version)

    @property
    def epoch(self):
        return self.changelog.epoch

    @property
    def upstream_version(self):
        if self.changelog:
            v = self.changelog.upstream_version
            return v if v else ''

    @property
    def _field_upstream_version(self):
        return str(self.upstream_version)

    @property
    def debian_version(self):
        return self.changelog.debian_version

    @property
    def _field_distribution(self):
        return self.changelog.distributions

    @property
    def distribution(self):
        return self._field_distribution

    @property
    def urgency(self):
        return self.changelog.urgency

    @property
    def _field_urgency(self):
        return self.urgency

    @property
    def last_changelog(self):
        if self._last_changelog is None:
            for i, block in enumerate(self.changelog):
                if i == 0:
                    # Skip the first one
                    continue
                if block.distributions.lower() == 'UNRELEASED':
                    # Ignore unreleased
                    continue
                self._last_changelog = block
                break
        return self._last_changelog

    @property
    def last_changelog_distribution(self):
        if self.last_changelog:
            return self.last_changelog.distributions

    @property
    def last_changelog_version(self):
        if self.last_changelog:
            return Version(self.last_changelog.version)

    @property
    def version_at_distribution(self):
        if self.last_changelog_distribution:
            return Version(util.version_at_distribution(
                self.source_name, self.last_changelog_distribution))

    def tag_template(self, name, version=None, vendor=None, **values):
        '''Returns a tag version correctly formatted according to the name.'''
        if version is None:
            version = self.version
        if vendor is None:
            vendor = self.get('vendor')
        try:
            v = Version(version)
            upstream_version = v.upstream_version
            v.epoch = None
            epochless_version = str(v)
        except ValueError:
            # version can be a glob like '*'
            upstream_version = version
            epochless_version = version

        values.setdefault('version', version)
        values.setdefault('debian_version', version)
        values.setdefault('upstream_version', upstream_version)
        values.setdefault('epochless_version', epochless_version)
        values.setdefault('vendor', vendor)
        values = {k: Version.to_tag(v) for k, v in values.items()}

        if name == 'upstream':
            return self.get('upstream_tag').format(**values)
        if name == 'debian':
            return self.get('debian_tag').format(**values)

    def uscan(self):
        return Uscan(self.path, destdir=self.tarball_dir)

    def is_native(self):
        return self.version and not self.version.debian_revision

    def is_dfsg(self):
        if self.is_native():
            return False
        if self.upstream_version and dfsg_re.match(self.upstream_version):
            return True
        return False

    def _append_field(self, control, dest, field):
        if field in control:
            dest.append(control[field])
        return dest

    @property
    def build_depends(self):
        control = self.source_control
        deps = []

        self._append_field(control, deps, 'Build-Depends')
        self._append_field(control, deps, 'Build-Depends-Indep')
        return ', '.join(deps)

    @property
    def runtime_depends(self):
        deps = []

        for control in self.binary_controls:
            self._append_field(control, deps, 'Pre-Depends')
            self._append_field(control, deps, 'Depends')

        return ', '.join(deps)

    @property
    def runtime_recommends(self):
        deps = []

        for control in self.binary_controls:
            self._append_field(control, deps, 'Recommends')

        return ', '.join(deps)

    @property
    def tests_depends(self):
        deps = []
        if not self.has_tests():
            return ''

        for control in self.tests_control:
            depends = control.get('Depends', '')
            depends_list = re.split(r'\s*,\s*', depends)
            restrictions = control.get('Restrictions', '')
            restrictions_list = re.split(r'\s*,\s*', restrictions)
            if '@' in depends_list:
                depends_list.remove('@')
                deps.append(self.runtime_depends)
                if 'needs-recommends' in restrictions_list:
                    deps.append(self.runtime_recommends)
            if '@builddeps@' in depends_list:
                depends_list.remove('@builddeps@')
                deps.append(self.build_depends)

            deps.extend(depends_list)

        return ', '.join(deps)

    def _all_depends(self):
        # return ', '.join([self.build_depends, self.runtime_depends, self.tests_depends])
        # Test dependencies might break the DAG
        return ', '.join([self.build_depends, self.runtime_depends])

    def internal_dependencies(self, binaries, dependencies=None):
        '''Process dependencies against binaries to take into account.

        :binaries: is a dictionary that maps binaries to the package_name
                   that generates it.
        :dependencies: is a dpkg depends string
        :returns: a mapping, of the package_names that generate the needed
                  binaries, and the binaries that cause the dependency as the
                  value.
        '''
        bin_deps = util.OrderedSet()
        if dependencies is None:
            dependencies = self._all_depends()
        rels = deb822.PkgRelation.parse_relations(dependencies)
        for or_part in rels:
            for part in or_part:
                bin_deps.add(part['name'])
        internal_dep = collections.OrderedDict()
        for dep in bin_deps:
            if dep not in binaries:
                continue
            if self.name == binaries[dep]:
                continue
            internal_dep.setdefault(binaries[dep], []).append(dep)
        return internal_dep

    def get_packages(self):
        'A simple dh_listpackages'
        packages = []
        for part in self.control:
            if 'Package' in part:
                packages.append(part['Package'])

        return packages

    def commit(self, msg, files):
        dch = get_dch(self)
        dch.commit(self, msg, files)

        # Just changed the changelog, but its probably not going to be used
        # anymore.
        self._changelog = None

    def create_branch(self, branch, tracking=None):
        if not tracking:
            # Search a remote to track
            trackable = []

            for remote in self.repo.remotes:
                remote.fetch(tags=True)
                if self.vcs_git and self.vcs_git == remote.url:
                    logging.debug('%s: remote %s configured as vcs-git',
                                  self.name, remote.name)
                    tracking = remote
                    break

                if branch in remote.refs:
                    logging.debug('%s: found %s/%s', self.name, remote.name,
                                  branch)
                    trackable.append(remote)

            if not tracking and trackable:
                tracking = trackable[0]

        if tracking and branch in tracking.refs:
            # Create a new branch with tracking
            head = self.repo.create_head(branch, commit=tracking.refs[branch])
            head.set_tracking_branch(tracking.refs[branch])
            return head
        else:
            return False

    def switch_branches(self, branch):
        ignore_new = self.get('ignore_new')
        if not ignore_new and self.repo.is_dirty(untracked_files=True):
            logging.warning(
                '%(pname)s: branch %(curr)s has uncommitted changes.'
                'Can\'t switch to %(dst)s from %(curr)s',
                pname=self.name, curr=self.branch, dst=branch)
            return False
        try:
            self.git.checkout(branch)
        except git.exc.GitCommandError:
            # Does the branch even exists?
            logging.error(
                '%(pname)s: Can\'t switch to %(curr)s from %(dst)s.',
                pname=self.name, curr=self.branch, dst=branch)
            return False
        # TODO: Check if really needed
        self._changelog = None
        return True

    def _ahead_behind_rev_list(self, ref1, ref2):
        out = self.git.rev_list('--left-right', '--count',
                                '%s...%s' % (ref1, ref2))
        return map(int, out.split())

    def _ahead_behind_guess_local(self, local):
        if not local and not self.repo.head.is_detached:
            local = self.repo.active_branch
        if not local:
            # FIXME: it might be wiser to use HEAD, but then we have to deal
            # with fake references
            local = self.debian_branch
        if isinstance(local, str):
            local = self.repo.references[local]
        return local

    def _get_default_remote(self):
        # repo.remote() fallsback to origin, even if there is no origin
        if not self.repo.remotes:
            return
        if (len(self.repo.remotes) == 1 or
                'origin' not in {r.name for r in self.repo.remotes}):
            return self.repo.remotes[0]
        return self.repo.remote()

    def _guess_tracking(self, local, tracking=None):

        if not tracking:
            tracking = local.tracking_branch()
            if not tracking:
                git_remote = self._get_default_remote()
                if not git_remote:
                    return
                tracking = git_remote.refs[local.name]
        if isinstance(tracking, str):
            tracking = self.repo.references[tracking]
        return tracking

    def ahead_behind(self, local=None, remote=None):
        # Hacky check between two refs
        # returns two numbers, commits ahead and commits behind
        local_ref = self._ahead_behind_guess_local(local)
        remote_ref = self._guess_tracking(local_ref, remote)
        if not remote_ref:
            # No remote to compare to
            return 0, 0
        return self._ahead_behind_rev_list(local_ref.path, remote_ref.path)

    def prepare_overlay(self, overlay_branch=None):
        '''Combines the upstream and debian branches into one working tree.

        If the debian_branch of the package already contains the upstream source
        (stored in self.merge attribute), then that branch is checked out,
        nothing else happens.

        If the package is a native package, the debian branch is checked out,
        nothing else happens.

        Args:
            overlay_branch: if received, the result is checked into a new branch
                instead of a dettached HEAD.
        '''

        # Try to switch to debian branch. If this fails, there are probably
        # uncommitted changes.
        logging.debug(
            '%s: switching to branch %s for overlay creation.',
            self.name, self.debian_branch)
        if not self.switch_branches(self.debian_branch):
            raise GitBranchError(
                'Could not change to branch {}'.format(self.debian_branch))
        if overlay_branch is None:
            overlay_branch = self.get(
                'overlay_branch',
                overlay_branch=vars(self.config.args).get('overlay_branch'))
        # Create working branch, if the user so requested
        if overlay_branch:
            logging.debug(
                '%s: creating new overlay branch %s.',
                self.name, overlay_branch)
            self.repo.create_branch('overlay_branch',
                                    self.repo.head.get_object())

        if self.is_merged:
            logging.info(
                '%s: skipping overlay creation, already merged.',
                self.name)
            return

        if self.is_native():
            logging.info(
                '%s: native package, no overlay needed.',
                self.name)
            return

        # Checkout the upstream tree into the current branch, then reset the
        # index so that the files are there, but not scheduled to be committed.
        logging.info(
            '%s: copying tree from latest upstream tag.',
            self.name)
        self.git.checkout(
            self.tag_template('upstream', self.upstream_version),
            '.', ':!.gitignore', ours=True)
        self.git.reset()
        logging.info(
            '%s: overlay created, branch needs to be manually cleaned',
            self.name)

    def _get_builder(self):
        distribution = self.builder_distribution
        if distribution.lower() == 'unreleased':
            distribution = 'sid'
        architecture = self.architecture
        builder_name = self.get('builder')

        return self._arriero.get_builder(builder_name,
                                         distribution,
                                         architecture)

    def build(self, ignore_branch=None):

        if ignore_branch is None:
            ignore_branch = self.get('ignore_branch')

        if not ignore_branch and self.debian_branch != self.branch:
            if not self.switch_branches(self.debian_branch):
                raise(GitBranchError('Could not change to branch %s' % (
                    self.debian_branch,)))

        builder = self._get_builder()

        if not self.get('source_only'):
            builder.ensure_image()

        builder.build(package=self,
                      ignore_branch=ignore_branch)

    def tag(self):

        if not self.get('tag'):
            return

        ignore_branch = self.get('ignore_branch')

        if not ignore_branch and self.debian_branch != self.branch:
            if not self.switch_branches(self.debian_branch):
                raise(GitBranchError('Could not change to branch %s' % (
                    self.debian_branch,)))

        builder = self._get_builder()
        builder.tag(package=self, ignore_branch=ignore_branch)

    def _get_tester(self):
        distribution = self.builder_distribution
        if distribution.lower() == 'unreleased':
            distribution = 'sid'
        architecture = self.architecture
        tester_name = self.get('tester')
        return self._arriero.get_tester(tester_name,
                                        distribution,
                                        architecture)

    def test(self, changes_file=None, ignore_branch=None, shell=NO,
             run_lintian=None, run_piuparts=None, run_autopkgtest=None):

        if ignore_branch is None:
            ignore_branch = self.get('ignore_branch')

        if not ignore_branch and self.debian_branch != self.branch:
            if not self.switch_branches(self.debian_branch):
                raise(GitBranchError('Could not change to branch %s' % (
                    self.debian_branch,)))

        if changes_file is None:
            changes_file = self.changes_file
        if not changes_file:
            logging.error('%s: Couldn\'t find the corresponding changes file',
                          self.name)
            return ERROR

        tester = self._get_tester()

        tester.update()

        return tester.test(package=self,
                           changes_file=changes_file,
                           shell=shell,
                           run_lintian=run_lintian,
                           run_piuparts=run_piuparts,
                           run_autopkgtest=run_autopkgtest)

    def _release_args(self, distribution, pre_release, ignore_branch):

        if pre_release is None:
            pre_release = self.get('pre_release')
        if pre_release:
            distribution = 'UNRELEASED'
        if distribution is None:
            # Use the cli option first
            distribution = self.get('target_distribution')
        if ignore_branch is None:
            ignore_branch = self.get('ignore_branch')
        return distribution, pre_release, ignore_branch

    def release(self, distribution=None, pre_release=None, ignore_branch=None):

        distribution, pre_release, ignore_branch = \
            self._release_args(distribution, pre_release, ignore_branch)

        if not ignore_branch and self.debian_branch != self.branch:
            if not self.switch_branches(self.debian_branch):
                raise(GitBranchError('Could not change to branch %s' % (
                    self.debian_branch,)))

        unreleased = self.distribution.lower() == 'unreleased'

        dch = get_dch(self)
        if pre_release:
            return dch.pre_release(self, unreleased, ignore_branch)
        else:
            return dch.release(self, distribution, unreleased, ignore_branch)

    def upload(self, host=None, force=None, changes_file=None):
        if host is None:
            host = self.get('upload_host')
        if force is None:
            force = self.get('force')
        if changes_file is None:
            changes_file = self.changes_file

        upload_command = self.config.get(
            'upload_command', section=self.name, raw=True)

        if not upload_command:
            return ERROR
        if not changes_file:
            return IGNORE

        sign_upload = self.get('sign_upload')
        if sign_upload and changes_file:
            ret = util.quiet(['gpg', '--verify', changes_file])
            signed = (ret == 0)
            if not signed:
                cmd = ['debsign', changes_file]
                util.log_check_call(cmd, interactive=True, cwd=self.path)

        cmd_variables = {
            'changes_file': changes_file,
            'package': self.name,
            'version': self.version,
            'target_distribution': self.target_distribution,
            'distribution': self.distribution,
            'dist': self.distribution,
            'upload_host': host,
        }

        try:
            full_command = upload_command.format(**cmd_variables)
        except (ValueError, KeyError) as e:
            logging.error('%s: unable to format upload-command: %s',
                          self.name, upload_command)
            logging.error('%s: %s' % (e.__class__.__name__, e.message))
            return ERROR

        util.log_check_call(full_command, interactive=True, shell=True,
                            cwd=self.path)

    @staticmethod
    def _branch_to_ref(branch):
        return 'refs/heads/{}'.format(branch)

    def _get_tracking_remote_name(self, branch, fallback_to_default=True):
        try:
            if isinstance(branch, str):
                if branch not in self.repo.branches:
                    return
                branch = self.repo.branches[branch]
            tracked = branch.tracking_branch()
            if tracked:
                remote_name = tracked.remote_name
            else:
                if not fallback_to_default:
                    return
                remote = self._get_default_remote()
                if not remote:
                    return
                remote_name = remote.name
            return remote_name
        except git.exc.GitCommandError as e:
            logging.debug('%s: No remote associated.\n%s', self.name, e)

    def _push(self, branch, tag_template=None, fallback=True):

        remote_name = self._get_tracking_remote_name(branch,
                                                     fallback_to_default=fallback)
        if not remote_name:
            return True

        ref = self._branch_to_ref(branch)
        try:
            ref += ":{}".format(
                self.git.config('branch.%s.merge' % branch))
        except git.exc.GitCommandError as e:
            logging.debug('%s: No remote merge ref associated.\n%s', self.name, e)

        if tag_template:
            tag_refs = 'refs/tags/%s' % self.tag_template(tag_template, '*')

        try:
            self.git.push(remote_name, ref)
            if tag_template:
                self.git.push(remote_name, tag_refs)
        except git.exc.GitCommandError as e:
            logging.error('%s: Failed to push.\n%s', self.name, e)
            return False

        return True

    def push(self, debian_tags=None, upstream_push=None):

        logging.debug('Pushing %s', self.name)
        if debian_tags is None:
            debian_tags = self.get('debian_tags')
        if upstream_push is None:
            upstream_push = self.get('upstream_push')

        if not self._push(self.debian_branch,
                          'debian' if debian_tags else ''):
            return False
        if not self._push(self.upstream_branch, 'upstream', fallback=upstream_push):
            return False
        if self.pristine_tar:
            if not self._push(self.pristine_tar_branch, fallback=upstream_push):
                return False
        return True

    def safe_pull(self):
        # update to check ahead/behind
        # TODO: fix ugly hack
        if not self.branch:
            logging.info('Index is in a detached head')
            return False

        remote_name = self._get_tracking_remote_name(self.repo.active_branch)
        if not remote_name:
            return True

        try:
            self.git.fetch(remote_name, tags=True)
        except git.exc.GitCommandError as e:
            logging.info('Error on fetch: %s', e)
            return False

        try:
            ahead, behind = self.ahead_behind(self.repo.active_branch)
            if ahead > 0 and behind > 0:
                raise(GitDiverge('Needs to merge with head.'))
        except GitRemoteNotFound as e:
            logging.info('Branch not associated with a remote: %s' %
                         e.message)
            return False

        # TODO: return True if there were changes
        try:
            self.git.pull()
        except git.exc.GitCommandError as e:
            logging.error('%s: Failed to pull.\n%s', self.name, e)
            raise(GitDirty(str(e)))

    def _pull_branch(self, branch):
        # FIXME: Know issue
        #        if we have local changes in a branch that's not the
        #        current one it fails with rejected * (non-fast-forward)
        # We could detect if we only have local changes and ignore the
        # pull, but we will eventually hit the case when both the remote
        # and the local branch have changes.
        # For that could either change to the branch (which might be tough if
        # the worktree has uncommitted changes, ignored files, etc), use a
        # different worktree for the merge. Both of these are quite
        # frail.
        try:
            branch_ref = self.repo.branches[branch]
            tracking_ref = self._guess_tracking(branch_ref)
            if not tracking_ref:
                raise IndexError('No remote asociated with {}'.format(branch_ref))

            remote_ref = self.repo.remotes[tracking_ref.remote_name]
            tracking = self._branch_to_ref(tracking_ref.remote_head)

            logging.debug(
                '%s %s %s:%s',
                'pull' if branch == self.branch else 'fetch',  # action
                remote_ref.name,
                tracking,
                branch_ref.path)
            remote_ref.fetch(tags=True)
            if branch == self.branch:
                remote_ref.pull()
            else:
                ahead, behind = self.ahead_behind(branch_ref, tracking_ref)
                if ahead == 0:
                    remote_ref.fetch('{}:{}'.format(tracking, branch_ref.path))
                if ahead and behind:
                    raise(GitDirty('failed to pull {}, needs to be '
                                   'manually merged'.format(self.name,
                                                            branch)))
        except IndexError as e:
            logging.warn('%s: Failed to access remote branch asociated with '
                         '%s', self.name, branch)
            logging.warn('%s: %s', self.name, e)
            return False
        except git.exc.GitCommandError as e:
            logging.error('%s: Failed to pull.', self.name)
            logging.error('%s: %s', self.name, e)
            return False
        return True

    def pull(self):

        logging.debug('Pulling %s', self.name)
        if not self._pull_branch(self.debian_branch):
            return False
        if not self._pull_branch(self.upstream_branch):
            return False
        if self.pristine_tar:
            if not self._pull_branch(self.pristine_tar_branch):
                return False
        return True

    def get_new_version(self, upstream):
        ''' Obtains a new version.
        '''
        old = Version(self.version)
        new = old.new_upstream_version(upstream)
        return new.full_version

    def new_dfsg_version(self, upstream_version):
        # copyright excluded files rules are handled by uscan
        if upstream_version.rstrip('0123456789').endswith('+dfsg'):
            return upstream_version
        branch = self.branch
        rules = os.path.join(self.path, 'debian', 'rules')
        dfsg_version = upstream_version + '+dfsg'
        dfsg_tag = self.tag_template('upstream', dfsg_version)

        if dfsg_tag in self.repo.tags:
            # Already there
            return dfsg_version

        self.git.checkout(self.tag_template('upstream', upstream_version))
        self.git.checkout('heads/%s' % branch, '--', 'debian')
        self.git.reset()
        util.log_check_call(
            ['fakeroot', rules, 'prune-nonfree'],
            cwd=self.path)
        util.log_check_call(['rm', '-rf', 'debian'], cwd=self.path)
        if (not self.repo.is_dirty(untracked_files=True)):
            # No changes made by the prune-nonfree call, we are free \o/
            self.switch_branches(branch)
            return upstream_version
        self.git.commit('-a', '-m', 'DFSG version %s' % (dfsg_version,))
        self.repo.create_tag(dfsg_tag)
        self.switch_branches(branch)
        return dfsg_version

    def _create_upstream_branch(self):
        if not self.upstream_branch or self.is_native():
            return IGNORE

        if self.upstream_branch not in self.repo.branches:
            logging.debug('Creating upstream branch for %s.', self.name)
            original_branch = self.branch

            # Create upstream branch
            self.git.checkout('--orphan', self.upstream_branch)
            self.git.reset()
            self.git.clean('-xdf')
            self.git.commit('--allow-empty', '-m', 'Upstream branch')
            self.switch_branches(original_branch)
        return OK

    def fetch_upstream(self, ignore_branch=None):
        '''Fetch upstream tarball when there is no upstream_branch.'''

        if ignore_branch is None:
            ignore_branch = self.get('ignore_branch')

        status = self._create_upstream_branch()
        if status == OK:
            status = self._get_upstream_release(current=True,
                                                version=self.upstream_version,
                                                ignore_branch=ignore_branch)
        return status

    def get_upstream_release_uscan(self, current=False, version=None):
        '''Download the upstream tarball with uscan.

        Args:
            current: if True downloads the tarball even if it's the same as
                     the one in the changelog file.
        '''
        try:
            pkg_scan = self.uscan()
            pkg_scan.scan(download=True, force_download=current,
                          version=version)
        except UscanError as e:
            logging.error(
                '%s: Could not download upstream tarball: %s',
                self.name, str(e))
            return False

        # ignore uptodate status if we forced the download
        if not current and pkg_scan.uptodate:
            return False

        return (pkg_scan.version, pkg_scan.tarball)

    def _get_local(self, file_glob, version_re, requested_version):
        files = []

        for filename in glob.iglob(
                os.path.join(self.tarball_dir, file_glob)):
            basename = os.path.basename(filename)
            # Skip .gpg .sig .pgp files
            if basename.endswith('.gpg') \
                    or basename.endswith('.sig') \
                    or basename.endswith('.pgp'):
                continue
            m = re.search(version_re, basename)
            if not m:
                continue
            version = m.group(1)
            if requested_version:
                if ds.version_compare(version, requested_version) == 0:
                    logging.debug('%s: %s found', self.name, filename)
                    files.append((version, filename))
            elif ds.version_compare(version, self.upstream_version) > 0:
                logging.debug('%s: %s found', self.name, filename)
                # is already there?
                upstream_tag = self.tag_template('upstream', version)
                if upstream_tag in self.repo.tags:
                    continue
                files.append((version, filename))

        if files:
            files.sort(key=lambda x: Version(x[0]),
                       reverse=True)
            version = files[0][0]
            return version, (file[1] for file in files if version == file[0])

        return None

    def get_upstream_release_pristine_tar(self, version):

        if not version or not self.pristine_tar:
            return
        cmd = ['pristine-tar', 'list']
        result = util.log_run(cmd, cwd=self.path)
        if result.returncode:
            return
        tarballs = []
        for line in util.iter_lines(result.stdout):
            line = line.rstrip('\n')
            m = re.search(r'{}_{}.orig'.format(
                self.source_name, version), line)
            if m:
                tarballs.append(line)
        if tarballs:
            return version, tarballs

    def get_upstream_release_local(self, version=None):
        '''Check if new upstream release file is already downloaded.'''
        requested_version = version

        logging.debug('%s: looking for .orig files.', self.name)

        file_glob_orig = self.source_name + '_[0-9]*.orig*.tar.*'
        version_orig_re = r'_([0-9.]+)\.'
        found = self._get_local(file_glob_orig, version_orig_re, requested_version)
        if found:
            return found

        logging.debug('%s: looking for non .orig files.', self.name)
        # not found, let's see if the file is downloaded without the .orig name
        copyright_file = open(os.path.join(self.path, 'debian', 'copyright'))
        copyright_deb822 = deb822.Deb822(copyright_file)
        if copyright_deb822 and 'Upstream-Name' in copyright_deb822:
            upstream_name = copyright_deb822['Upstream-Name']
        else:
            upstream_name = self.source_name

        file_glob_other = upstream_name + '[-_][0-9]*.tar.*'
        version_re = r'[-_]([0-9.]+)\.tar'
        found = self._get_local(file_glob_other, version_re, requested_version)
        if found:
            return found

        if upstream_name != self.source_name:
            logging.debug('%s: looking for non .orig files using source_name.',
                          self.name)
            file_glob_other = self.source_name + '[-_][0-9]*.tar.*'
            version_re = r'[-_]([0-9.]+)\.tar'
            found = self._get_local(file_glob_other, version_re, requested_version)
            if found:
                return found

        return False

    def _get_upstream_release_download(self, version, current):

        download = self.get_upstream_release_pristine_tar(version=version)
        if not download:
            download = self.get_upstream_release_local(version=version)
        if not download:
            if not current:
                # if we don't have the current version, fetch at least that
                upstream_tag = self.tag_template('upstream',
                                                 self.upstream_version)
                current = upstream_tag not in self.repo.tags
            download = self.get_upstream_release_uscan(current=current,
                                                       version=version)
        return download

    def _create_empty_orig(self, upstream_version):
        orig = '{}/{}_{}.orig.tar.xz'.format(self.tarball_dir,
                                             self.source_name,
                                             upstream_version)
        cmd = ['tar', 'cJf', orig, '-T', '/dev/null']
        util.log_check_call(cmd, cwd=self.path)
        return orig

    def _get_upstream_release_call_import_orig_components(self, cmd,
                                                          tarballs,
                                                          upstream_version):
        components = set()
        orig = None
        for tarball in tarballs:
            m = re.search(r'orig-(.*?)\.', tarball)
            if m:
                component = m.group(1)
                logging.debug('%s: component %s found', self.name,
                              component)
                components.add(component)
            elif not orig:
                orig = tarball
        if not orig:
            orig = self._create_empty_orig(upstream_version)
        for c in components:
            cmd.append('--component={}'.format(c))
        cmd.append(orig)

    def _get_upstream_release_call_import_orig(self, upstream_version,
                                               tarballs):

        cmd = ['gbp', 'import-orig',
               '--upstream-version=%s' % upstream_version]

        # TODO: this should not be duplicated
        if self.debian_branch:
            cmd.append('--debian-branch=%s' % self.debian_branch)
        if self.upstream_branch:
            cmd.append('--upstream-branch=%s' % self.upstream_branch)
        if self.pristine_tar:
            cmd.append('--pristine-tar')
            if self.filter_orig:
                cmd.append('--filter-pristine-tar')
                for pattern in self.filter_orig:
                    cmd.append('--filter=%s' % pattern)
        else:
            cmd.append('--no-pristine-tar')
        if self.is_merged:
            cmd += ['--merge', '--merge-mode=replace']
        else:
            cmd.append('--no-merge')
        upstream_vcs_tag = self.get('upstream_vcs_tag')
        if upstream_vcs_tag:
            # We need the tags from the upstream vcs
            self.git.fetch('--all', '--tags')
            cmd.append('--upstream-vcs-tag={}'.format(upstream_vcs_tag))

        self._get_upstream_release_call_import_orig_components(cmd, tarballs,
                                                               upstream_version)

        return util.log_check_call(cmd, cwd=self.path)

    def _get_upstream_release(self, ignore_branch=False,
                              current=False, version=None):

        if self.is_native():
            return False

        logging.debug('%s: Fetching upstream tarball.', self.name)

        if self.repo.is_dirty(untracked_files=True):
            raise(GitDirty('Uncommited changes'))

        if not ignore_branch and self.debian_branch != self.branch:
            if not self.switch_branches(self.debian_branch):
                raise(GitDirty('Could not switch branches'))

        self.safe_pull()
        # Need to reread changelog
        self.update_changelog()

        if version and dfsg_re.match(version):
            version = dfsg_re.sub(r'\1', version)

        # Let's check if it's already downloaded
        download = self._get_upstream_release_download(version, current)
        if not download:
            return False
        upstream_version, tarballs = download

        tag = self.tag_template('upstream', upstream_version)
        if tag not in self.repo.tags:
            self._get_upstream_release_call_import_orig(upstream_version,
                                                        tarballs)

        # TODO: if requested version is dfsg, it should apply the fixes
        #       corresponding to that version
        if self.is_dfsg():
            return self.new_dfsg_version(upstream_version)
        return upstream_version

    def _upstream_changes_process_args(self, old_version, new_version,
                                       ignore_branch):
        if ignore_branch is None:
            ignore_branch = self.get('ignore_branch')

        if not old_version and not new_version:
            sections = self.changelog.sections
            new_version = self.upstream_version
            for section in sections:
                version = Version(section.version)
                if version.upstream_version != new_version:
                    old_version = version.upstream_version
                    break
        elif not old_version:
            old_version = self.upstream_version
        elif not new_version:
            new_version = self.upstream_version
        return old_version, new_version, ignore_branch

    def upstream_changes(self, old_version=None, new_version=None,
                         ignore_branch=None):

        logging.debug('%s: Checking changes between upstream releases', self.name)
        old_version, new_version, ignore_branch = \
            self._upstream_changes_process_args(
                old_version, new_version, ignore_branch)

        if not old_version:
            return True

        old_tag = self.tag_template('upstream', old_version)
        new_tag = self.tag_template('upstream', new_version)

        # Are this versions imported and tagged?
        if old_tag not in self.repo.tags:
            self._get_upstream_release(version=old_version,
                                       ignore_branch=ignore_branch)
        if new_tag not in self.repo.tags:
            self._get_upstream_release(version=new_version,
                                       ignore_branch=ignore_branch)
        if old_tag not in self.repo.tags:
            # still not there, nothing to compare
            return True

        return self.repo.tags[old_tag].commit.tree.diff(new_tag)

    def new_upstream_release(self, ignore_branch=None):

        logging.debug(
            '%s: Searching for a new upstream release', self.name)
        if ignore_branch is None:
            ignore_branch = self.get('ignore_branch')

        request_version = self.get('request_version')

        if not self.check_path():
            logging.error(
                '%s: %s doesn\'t exist', self.name, self.path)
            return False

        status = self._create_upstream_branch()
        if status == OK:
            # Start by fetching the current version, if it's already there it
            # should be a noop
            self._get_upstream_release(current=True,
                                       version=self.upstream_version,
                                       ignore_branch=ignore_branch)
            upstream_version = self._get_upstream_release(
                current=False,
                version=request_version,
                ignore_branch=ignore_branch)
        else:
            return status == IGNORE

        if not upstream_version:
            logging.debug('%s: No upstream version found.', self.name)
            return False

        # upstream_changes imports the corresponding upstream tags
        if not self.upstream_changes(new_version=upstream_version,
                                     ignore_branch=ignore_branch):
            return False

        if self.upstream_version == upstream_version:
            return False

        version = self.get_new_version(upstream_version)

        msg = 'New upstream release ({}).'.format(upstream_version)
        dch = get_dch(self)
        dch.new_version(self, msg, version)

        # Just changed the changelog, but its probably not going to be used
        # anymore.
        self._changelog = None

        return True

    def get_status(self):

        status = []
        status.append('Package: {}'.format(self.name))
        status.append('+ Directory: {}'.format(self.path))

        if not self.check_path():
            status.append('! Status: Error, "{}" doesn\'t exist'.format(
                self.path))
            return status

        status.extend(self._get_status_branch())
        return status

    def _get_status_switch_branches(self, status):

        # Check if there are non commited changes
        dirty = self.repo.is_dirty(untracked_files=True)
        if dirty:
            status.append('! Status: Uncommited changes')
            status.append(self.git.status())

        # Check is head is detached
        if not self.branch:
            status.append('! Status: HEAD is detached')

        # Check current version (only in debian)
        if not self.branch or self.branch not in self.debian_branch:
            if dirty:
                status.append('! Status: Can\'t check version, dirty branch')
                return False
            # If it's not dirty we can change branches, right?
            if not self.switch_branches(self.debian_branch):
                status.append(
                    '! Status: Error, change to branch {} failed'.format(
                        self.debian_branch))
                return False
            else:
                status.append('* Switched to branch: {}'.format(self.branch))

        return True

    def _get_status_branch(self):

        status = []
        status.append('+ Branch: {}'.format(self.branch))

        error = False

        branches = {'debian': self.debian_branch}
        if self.pristine_tar:
            branches['pristine-tar'] = self.pristine_tar_branch

        if not self._get_status_switch_branches(status):
            return status

        status.append('+ Version: {}'.format(self.version))

        # Now that we are in the debian branch we can check if the package is
        # native or not.
        if not self.is_native():
            branches['upstream'] = self.upstream_branch

        for k, v in branches.items():
            if v not in self.repo.heads:
                status.append('! Status: Error, Missing {} branch: {}'.format(
                    k, v))
                error = True
        if error:
            return status

        # Check upstream tag with current version
        # TODO:
        # status.append(self.tag_template('upstream'))
        # Check if released
        # released = (self.distribution.lower() != 'unreleased')
        status.append('+ Distribution: {}'.format(self.distribution))
        # if released, check if tagged in debian repo
        # TODO:
        # status.append(self.tag_template('debian'))

        # TODO: it should be possible to obtain the state without actually
        # changing branches
        status.extend(self._get_status_uscan())
        status.extend(self._get_status_repo())
        status.extend(self._get_status_build())

        return status

    def _get_status_uscan(self):
        '''Check if the package is up to date with uscan.'''

        # Native packages don't have uscan status
        if self.is_native():
            return []

        try:
            uscan_status = self.uscan()
            uscan_status.scan(download=False)
        except UscanError as e:
            return ['! Status: Error while running uscan: {}'.format(str(e))]

        status = []
        if not uscan_status.uptodate:
            status.append(
                '! Status: New upstream release available.\n'
                'Local version: {0.version}.\n'
                '! Upstream version: {0.upstream_version}.\n'
                'Status: Source URL: {0.url}'.format(uscan_status)
            )
            if uscan_status.tarball:
                status.append('- Source: Already downloaded in {}'.format(
                    uscan_status.tarball)
                )
        return status

    def _get_status_repo(self):
        # Check if up to date with git repo
        status = []
        remote_name = self._get_tracking_remote_name(self.repo.active_branch)
        if not remote_name:
            status.append('! Status: Branch not associated with a remote')
            return status

        try:
            self.git.fetch(remote_name, tags=True)
            ahead, behind = self.ahead_behind()
            if behind > 0:
                status.append(
                    '! Status: Remote changes commited ({}), pull them'.format(
                        behind))
            if ahead > 0:
                status.append(
                    '! Status: Local changes commited ({})'.format(ahead))
        except git.exc.GitCommandError:
            status.append('! Status: Branch not associated with a remote')
        return status

    def _get_status_build(self):

        status = []
        if not self.changes_file:
            status.append('- Status: The package has not been built')

        signed = False
        if self.changes_file:
            ret = util.quiet(['gpg', '--verify', self.changes_file])
            signed = (ret == 0)

        unreleased = (self.distribution.lower() == 'unreleased')
        if signed and not unreleased:
            if self.upload_files:
                status.append('+ Status: Package signed for {}, uploaded ({})'.format(
                    self.distribution, ' '.join(self.upload_files)))
            else:
                status.append(
                    '! Status: Package signed for {}, not uploaded'.format(
                        self.distribution))
        if self.changes_file and not signed:
            status.append('- Status: Package has been built but not signed')
        # Check lintian
        # Check for reported errors
        # Check errors reported upstream

        # tarball_dir
        # export_dir
        # self.changelog.version
        # self.changelog.epochless_version
        return status

    def symbols_files(self, ignore_branch=None):

        if ignore_branch is None:
            ignore_branch = self.get('ignore_branch')
        if not ignore_branch and self.debian_branch != self.branch:
            if not self.switch_branches(self.debian_branch):
                raise(GitBranchError('Could not change to branch {}'.format(
                    self.debian_branch)))
        return glob.glob(os.path.join(self.path, 'debian', '*.symbols'))

    def has_symbols(self, ignore_branch=True):
        return bool(self.symbols_files(ignore_branch=ignore_branch))

    def has_tests(self):
        dirname = os.path.join(self.path, 'debian', 'tests')
        filename = os.path.join(dirname, 'control')
        if not (os.path.isdir(dirname) and os.path.exists(filename)):
            return False
        with open(filename) as f:
            return len(list(deb822.Deb822.iter_paragraphs(f))) > 0

# vi:expandtab:softtabstop=4:shiftwidth=4:smarttab
